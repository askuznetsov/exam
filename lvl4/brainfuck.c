/*
Assignment name  : brainfuck
Expected files   : *.c, *.h
Allowed functions: write, malloc, free
--------------------------------------------------------------------------------

Write a Brainfuck interpreter program.
The source code will be given as first parameter.
The code will always be valid, with no more than 4096 operations.
Brainfuck is a minimalist language. It consists of an array of bytes
(in our case, let's say 2048 bytes) initialized to zero,
and a pointer to its first byte.

Every operator consists of a single character :
- '>' increment the pointer ;
- '<' decrement the pointer ;
- '+' increment the pointed byte ;
- '-' decrement the pointed byte ;
- '.' print the pointed byte on standard output ;
- '[' go to the matching ']' if the pointed byte is 0 (while start) ;
- ']' go to the matching '[' if the pointed byte is not 0 (while end).

Any other character is a comment.

Examples:

$>./brainfuck "++++++++++[>+++++++>++++++++++>+++>+<<<<-]
>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>." | cat -e
Hello World!$
$>./brainfuck "+++++[>++++[>++++H>+++++i<<-]>>>++\n<<<<-]>>--------.>+++++.>." | cat -e
Hi$
$>./brainfuck | cat -e
$
--------------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <unistd.h>

void 	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		go_brackets_one(char *str, int i)
{
	int count = 1;

	i++;
	while (count != 0)
	{
		if (str[i] == '[')
			count++;
		if (str[i] == ']')
			count--;
		i++;
	}
	return (i);
}

int		go_brackets_two(char *str, int i)
{
	int count = 1;

	i--;
	while (count != 0)
	{
		if (str[i] == ']')
			count++;
		if (str[i] == '[')
			count--;
		i--;
	}
	return (i);
}

void 	brainfuck(char *str, char *fuck)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '>')
			fuck++;
		else if (str[i] == '<')
			fuck--;
		else if (str[i] == '+')
			*fuck += 1;
		else if (str[i] == '-')
			*fuck -= 1;
		else if (str[i] == '.')
			ft_putchar(*fuck);
		else if (str[i] == '[' && *fuck == 0)
			i = go_brackets_one(str, i);
		else if (str[i] == ']' && *fuck != 0)
			i = go_brackets_two(str, i);
		i++;
	}
}

int		main(int argc, char **argv)
{
	char *fuck;
	int i;

	if (argc != 2)
	{
		ft_putchar('\n');
		return (0);
	}
	i = 0;
	fuck = (char*)malloc(sizeof(char) * 2048);
	while (i < 2048)
		fuck[i++] = 0;
	brainfuck(argv[1], fuck);
	return (0);
}
