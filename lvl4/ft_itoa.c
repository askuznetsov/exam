/*
Assignment name  : ft_itoa
Expected files   : ft_itoa.c
Allowed functions: malloc
--------------------------------------------------------------------------------

Write a function that takes an int and converts it to a null-terminated string.
The function returns the result in a char array that you must allocate.

Your function must be declared as follows:

char	*ft_itoa(int nbr);

--------------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>

int		count_int(int n)
{
	int i;

	i = 0;
	while (n != 0)
	{
		n = n / 10;
		i++;
	}
	return (i);
}

char	*ft_itoa(int nbr)
{
	int 			size;
	char			*ar;
	int 			i;
	unsigned int 	tea;

	i = 0;
	if (nbr < 0)
		tea = -nbr;
	else
		tea = nbr;
	if (nbr > 0)
		size = count_int(nbr);
	else
		size = count_int(nbr) + 1;
	printf("Size:  %d\n", size);
	ar = (char *)malloc(sizeof(char) * size);
	if (nbr < 0)
		ar[i] = '-';
	i = size;
	ar[i--] = '\0';
	while (tea > 0)
	{
		ar[i--] = (tea % 10) + '0';
		tea = tea / 10;
	}
	ar[i] = tea + '0';
	return (ar);
}

int		main(void)
{
	int		number = 6996;

	printf("Now, char_number is:  %s\n", ft_itoa(number));
	return (0);
}
