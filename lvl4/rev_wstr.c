/*
Assignment name  : rev_wstr
Expected files   : rev_wstr.c
Allowed functions: write, malloc, free
--------------------------------------------------------------------------------

Write a program that takes a string as a parameter, and prints its words in
reverse order.

A "word" is a part of the string bounded by spaces and/or tabs, or the
begin/end of the string.

If the number of parameters is different from 1, the program will display
'\n'.

In the parameters that are going to be tested, there won't be any "additional"
spaces (meaning that there won't be additionnal spaces at the beginning or at
the end of the string, and words will always be separated by exactly one space).

Examples:

$> ./rev_wstr "le temps du mepris precede celui de l'indifference" | cat -e
l'indifference de celui precede mepris du temps le$
$> ./rev_wstr "abcdefghijklm"
abcdefghijklm
$> ./rev_wstr "il contempla le mont" | cat -e
mont le contempla il$
$> ./rev_wstr | cat -e
$
$>
--------------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char temp)
{
	write(1, &temp, 1);
}

void	write_till(char *s, int i, int size)
{
	int temp;

	if (i > 0)
		temp = ++i;
	else
		temp = i;
	while (s[temp] != ' ' && temp < size)
		ft_putchar(s[temp++]);
	if (i != 0)
		ft_putchar(' ');
	else
		ft_putchar('\n');
}

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void	rev_wstr(char *str)
{
	int	i;
	int size;

	size = ft_strlen(str);
	i = ft_strlen(str);
	while (i >= 0)
	{
		if (str[i] == ' ' || i == 0)
			write_till(str, i, size);
		i--;
	}
}

int		main(int argc, char **argv)
{
	if (argc >= 3)
	{
		ft_putchar('\n');
	}
	else if (argc == 2)
	{
		rev_wstr(argv[1]);
	}
	return (0);
}
