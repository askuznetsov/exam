/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rostring.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okuznets <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/29 17:52:07 by okuznets          #+#    #+#             */
/*   Updated: 2018/08/29 18:04:26 by okuznets         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
/*Assignment name  : rostring
Expected files   : rostring.c
Allowed functions: write, malloc, free
--------------------------------------------------------------------------------

Write a program that takes a string and displays this string after rotating it
one word to the left.

Thus, the first word becomes the last, and others stay in the same order.

A "word" is defined as a part of a string delimited either by spaces/tabs, or
by the start/end of the string.

Words will be separated by only one space in the output.

If there's less than one argument, the program displays \n.

Example:

$>./rostring "abc   " | cat -e
abc$
$>
$>./rostring "Que la      lumiere soit et la lumiere fut"
la lumiere soit et la lumiere fut Que
$>
$>./rostring "     AkjhZ zLKIJz , 23y"
zLKIJz , 23y AkjhZ
$>
$>./rostring | cat -e
$
$>
--------------------------------------------------------------------------------
*/

#include <unistd.h>
#include <stdlib.h>

void	ft_putchar(char temp)
{
	write(1, &temp, 1);
}

int		first_word_start(char *str)
{
	int i;

	i = 0;
	while (str[i] == ' ' || str[i] == '\t')
		i++;
	return (i);
}

void	rostring(char *str)
{
	int	i;
	int	first_word_place;
	int	first_word_place_end;
	int	count;

	i = 0;
	count = 0;
	while (str[i] == ' ' || str[i] == '\t')
		i++;
	first_word_place = i;
	while (str[i] > 32 && str[i] < 127)
		i++;
	first_word_place_end = i;
	count++;
	while (str[i] == ' ' || str[i] == '\t')
		i++;
	while (str[i])
	{
		if (str[i] == '\t' || str[i] == '	' || str[i] == ' ')
		{
			while (str[i] == ' ' || str[i] == '\t')
				i++;
			ft_putchar(' ');
		}
		else
			ft_putchar(str[i++]);
		count++;
	}
	if (count > 1)
		ft_putchar(' ');
	while (first_word_place < first_word_place_end)
		ft_putchar(str[first_word_place++]);
}

int		main(int argc, char **argv)
{
	if (argc != 2)
	{
		ft_putchar('\n');
		return (0);
	}
	rostring(argv[1]);
	ft_putchar('\n');
	return (0);
}
