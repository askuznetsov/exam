/*
Assignment name  : ft_split
Expected files   : ft_split.c
Allowed functions: malloc
--------------------------------------------------------------------------------

Write a function that takes a string, splits it into words, and returns them as
a NULL-terminated array of strings.

A "word" is defined as a part of a string delimited either by spaces/tabs/new
lines, or by the start/end of the string.

Your function must be declared as follows:

char    **ft_split(char *str);
--------------------------------------------------------------------------------
*/

int		count_words(char *str)
{
	int	i;
	int	n;

	i = 0;
	n = 0;
	while (str[i])
	{
		while (str[i] && (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'))
			i++;
		if (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
		{
			n++;
			while (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
				i++;
		}
	}
	return (n);
}

char	**fill_arr(char **arr, char *str)
{
	int	i;
	int	j;
	int	a;

	i = 0;
	j = 0;
	while (str[i])
	{
		while (str[i] && (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'))
			i++;
		if (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
		{
			a = 0;
			while (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
			{
				arr[j][a] = str[i];
				a++;
				i++;
			}
			arr[j][a] = '\0';
			j++;
		}
	}
	arr[j] = NULL;
	return (arr);
}

char	**alloc_mem(char **arr, char *str)
{
	int	i;
	int	j;
	int	a;

	i = 0;
	j = 0;
	while (str[i])
	{
		while (str[i] && (str[i] == ' ' || str[i] == '\t' || str[i] == '\n'))
			i++;
		if (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
		{
			a = 0;
			while (str[i] && str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
			{
				i++;
				a++;
			}
			arr[j] = (char*)malloc(sizeof(char) * (a + 1));
			j++;
		}
	}
	return (arr);
}

char	**ft_split(char *str)
{
	char	**arr;
	int		n;

	if (!str || str[0] == '\0')
	{
		arr = (char**)malloc(sizeof(char*) * 1);
		arr[0] = NULL;
		return (arr);
	}
	n = count_words(str);
	arr = (char**)malloc(sizeof(char*) * (n + 1));
	if (n == 0)
	{
		arr[0] = NULL;
		return (arr);
	}
	arr = alloc_mem(arr, str);
	arr = fill_arr(arr, str);
	return (arr);
}
